import COVID19Py
import telebot
from telebot import types
covid19 = COVID19Py.COVID19()
bot = telebot.TeleBot('5736435339:AAEuJ3jVUWgq7C9IOANhRp1JnB5o6Tqpr2Y')
latest = covid19.getLatest()


@bot.message_handler(commands=['start'])
def start(message):
    mess = f'Привет, <b>{message.from_user.first_name} <u>{message.from_user.last_name}!</u></b>\n' \
           f'Напишите город, в котором хотите знать количество зараженных:'
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
    btn1 = types.KeyboardButton("Во всем мире")
    btn2 = types.KeyboardButton("Россия")
    btn3 = types.KeyboardButton("Украина")
    btn4 = types.KeyboardButton("США")
    markup.add(btn1, btn2, btn3, btn4)
    bot.send_message(message.chat.id, mess, parse_mode='html', reply_markup=markup)


@bot.message_handler(content_types=['text'])
def get_user_text(message):
    final_message = ''
    if message.text == 'Россия':
        location = covid19.getLocationByCountryCode("RU")
    elif message.text == 'Украина':
        location = covid19.getLocationByCountryCode("UA")
    elif message.text == 'США':
        location = covid19.getLocationByCountryCode("US")
    else:
        location = covid19.getLatest()
        final_message = f"<u>Данные по всему миру:</u>\n<b>Заболевшие: </b>{location['confirmed']}\nСмертей: " \
                        f"{location['deaths']} "
    if final_message == '':
        date = location[0]['last_updated'].split("T")
        time = date[1].split(".")
        final_message = f"<u><b>Данные по стране: </b></u>\n" \
                        f"Население: {location[0]['country_population']}\n" \
                        f"Последнее обновление: {date[0]} {time[0]}\n" \
                        f"<b>Последние данные:</b> \n" \
                        f"Заболевших: {location[0]['latest']['confirmed']}\n" \
                        f"Смертей: {location[0]['latest']['deaths']}"
    bot.send_message(message.chat.id, final_message, parse_mode='html')


bot.polling(non_stop=True)
